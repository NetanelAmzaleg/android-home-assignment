package homework.chegg.com.chegghomework.network


import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import okhttp3.CacheControl
import java.io.File


object RetroService  {

    private const val BASE_URL = "https://chegg-mobile-promotions.cheggcdn.com/android/homework/"


//deceleration of retrofit using Gson as converterFactory
    val retrofitSource : Retrofit.Builder by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

    }

    //In case of another sources
    val api :Api by lazy {
        retrofitSource
            .build().create(Api::class.java)
    }
}