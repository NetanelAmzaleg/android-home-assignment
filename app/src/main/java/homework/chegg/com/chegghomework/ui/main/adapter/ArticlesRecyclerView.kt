package homework.chegg.com.chegghomework.ui.main.adapter

import android.content.Context

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

import com.squareup.picasso.Picasso
import homework.chegg.com.chegghomework.R

import homework.chegg.com.chegghomework.io.ioClass.SourceAEntity
import homework.chegg.com.chegghomework.io.ioClass.SourceBEntity
import homework.chegg.com.chegghomework.io.ioClass.SourceCEntity
import java.util.*


class ArticlesRecyclerView(
    var arrayList: ArrayList<Any>,
    val context: Context

) :
    RecyclerView.Adapter<ArticlesRecyclerView.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle: TextView = view.findViewById(R.id.tv_title)
        val tvSubtitile: TextView = view.findViewById(R.id.tv_subtitle)
        val imArticle: ImageView = view.findViewById(R.id.im_article)


    }


    interface OnItemClick {
        fun onItemClick(i: Int)

    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.row_list_article, viewGroup, false)

        return ViewHolder(view)
    }


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = this.arrayList[position]
        var title = ""
        var subtitle = ""
        var imageUrl = ""
        when(item){
            is SourceAEntity -> {
                title = item.title!!
                subtitle = item.subtitle!!
                imageUrl = item.imageUrl!!
            }
            is SourceBEntity -> {
                title = item.header!!
                subtitle = item.description!!
                imageUrl = item.picture!!
            }
            is SourceCEntity ->{
                title = item.topLine!!
                subtitle = item.subLine1 + "\n" + item.subline2!!
                imageUrl = item.image!!
            }
        }
        viewHolder.tvTitle.text = title
        viewHolder.tvSubtitile.text = subtitle
        Picasso.get().load(imageUrl).placeholder(R.drawable.notavailable).error(R.drawable.notavailable).into(viewHolder.imArticle)


    }

    override fun getItemCount(): Int {
        return arrayList.size
    }


}