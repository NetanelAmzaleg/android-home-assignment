package homework.chegg.com.chegghomework.ui.specific

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import homework.chegg.com.chegghomework.ui.main.MainRepo
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@HiltViewModel
class SpecificVM  @Inject constructor(
    //inject the repo into the view model
    private val specificRepo: SpecificRepo
)
    : ViewModel()  {

    suspend fun loadArticles(source :String) : LiveData<List<Any>?> = liveData {
        specificRepo.loadArticles(source).collect {
            emit(it)
        }
    }
}