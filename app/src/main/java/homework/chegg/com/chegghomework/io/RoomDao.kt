package homework.chegg.com.chegghomework.io

import androidx.lifecycle.LiveData
import androidx.room.*
import homework.chegg.com.chegghomework.io.ioClass.SourceAEntity
import homework.chegg.com.chegghomework.io.ioClass.SourceBEntity
import homework.chegg.com.chegghomework.io.ioClass.SourceCEntity
import homework.chegg.com.chegghomework.io.ioClass.TimeEntity


/**
 *  preparation to using room in case of need to save the data
 */

@Dao
interface RoomDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSourceAList(items: List<SourceAEntity>): List<Long>
    @Query("Delete from SourceAEntity")
    suspend fun deleteSourceA()
    @Query("Select *  from SourceAEntity")
    suspend fun getSourceA():List<SourceAEntity>
    @Query("Delete from TimeEntity where source =:source ")
    suspend fun deleteSourceTime(source : String)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSourceTime(timeEntity : TimeEntity)
    @Query("SELECT currentTime from TimeEntity where source =:source ")
    suspend fun getSourceTime(source : String) : String


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSourceBList(items: List<SourceBEntity>): List<Long>
    @Query("Delete from SourceBEntity")
    suspend fun deleteSourceB()
    @Query("Select *  from SourceBEntity")
    suspend fun getSourceB():List<SourceBEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSourceCList(items: List<SourceCEntity>): List<Long>
    @Query("Delete from SourceCEntity")
    suspend fun deleteSourceC()
    @Query("Select *  from SourceCEntity")
    suspend fun getSourceC():List<SourceCEntity>



}