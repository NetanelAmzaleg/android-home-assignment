package homework.chegg.com.chegghomework.io.ioClass

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(indices = [Index(value = ["id"], unique = true)])
@Parcelize
data class SourceCEntity(

    var image: String?,
    var subLine1: String?,
    var subline2: String?,
    var topLine: String?,
    @NonNull
    @PrimaryKey(autoGenerate = true)
    val id: Int?
): Parcelable



