package homework.chegg.com.chegghomework.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@HiltViewModel
class MainVM @Inject constructor(
    //inject the repo into the view model
    private val mainRepo: MainRepo
)
    : ViewModel()  {




    //load articles from repo
    suspend fun loadArticles() : LiveData<ArrayList<Any>?> = liveData {
       mainRepo.loadArticles().collect {
            emit(it)
        }
    }


}