package homework.chegg.com.chegghomework

import android.os.Bundle
import android.view.View
import android.view.animation.TranslateAnimation

import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.findNavController

import androidx.recyclerview.widget.LinearLayoutManager

import dagger.hilt.android.AndroidEntryPoint

import homework.chegg.com.chegghomework.databinding.ActivityMainBinding
import homework.chegg.com.chegghomework.ui.main.adapter.ArticlesRecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import android.animation.Animator

import android.animation.AnimatorListenerAdapter
import homework.chegg.com.chegghomework.ui.Consts


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    lateinit var navController: NavController
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //using view binding
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        navController = findNavController(R.id.nav_host_fragment_content_main)
        //handle item Selected in bottomNavigation
        binding.bottomNavigation.setOnItemSelectedListener { item ->
            when(item.itemId){

                R.id.sourceAItem ->{
                   navController.navigateUp()
                    val bundle = bundleOf("source" to Consts.DATA_SOURCE_A_URL)
                        navController.navigate(R.id.specificSourceFragment,bundle)
                    true
                }
                R.id.sourceBItem ->{
                    navController.navigateUp()
                    val bundle = bundleOf("source" to Consts.DATA_SOURCE_B_URL)
                    navController.navigate(R.id.specificSourceFragment,bundle)
                    true
                }
                R.id.sourceCItem ->{
                    navController.navigateUp()
                    val bundle = bundleOf("source" to Consts.DATA_SOURCE_C_URL)
                    navController.navigate(R.id.specificSourceFragment,bundle)
                    true
                }
                else -> false
            }
        }

// Start the animation of the bottomNavigation to slide up
        CoroutineScope(Main).launch {

            binding.bottomNavigation.visibility = View.VISIBLE;
            binding.bottomNavigation.alpha = 0.0f;
            binding.bottomNavigation.animate()
                .translationY(binding.bottomNavigation.height.toFloat())
                .alpha(1.0f)
                .setDuration(1500)
                .setListener(null)
        }

    }






}