package homework.chegg.com.chegghomework.classes

data class SourceAObj(
    val imageUrl: String?,
    val subtitle: String?,
    val title: String?
)