package homework.chegg.com.chegghomework.classes

data class Metadata(
    val innerdata: List<Innerdata>?,
    val `this`: String?
)