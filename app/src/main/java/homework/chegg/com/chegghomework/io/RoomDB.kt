package homework.chegg.com.chegghomework.io

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import homework.chegg.com.chegghomework.io.ioClass.SourceAEntity
import homework.chegg.com.chegghomework.io.ioClass.SourceBEntity
import homework.chegg.com.chegghomework.io.ioClass.SourceCEntity
import homework.chegg.com.chegghomework.io.ioClass.TimeEntity


@Database(entities = [SourceAEntity::class, SourceBEntity::class, SourceCEntity::class, TimeEntity::class],exportSchema = false,version =2)
//@TypeConverters(Converter::class)
abstract class RoomDB : RoomDatabase() {

    abstract fun roomDao() : RoomDao

    companion object{
         val DATABASE_NAME: String = "RoomDataBase"


        var INSTANCE: RoomDB? = null

        fun getAppDataBase(context: Context): RoomDB? {
            if (INSTANCE == null){
                synchronized(RoomDB::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext, RoomDB::class.java, "RoomDataBase")
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE
        }

    }

}
