package homework.chegg.com.chegghomework.classes

data class SourceCItem(
    val image: String?,
    val subLine1: String?,
    val subline2: String?,
    val topLine: String?
)