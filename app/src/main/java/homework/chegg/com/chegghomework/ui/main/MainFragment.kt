package homework.chegg.com.chegghomework.ui.main

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import homework.chegg.com.chegghomework.R
import homework.chegg.com.chegghomework.classes.Innerdata
import homework.chegg.com.chegghomework.classes.SourceAObj
import homework.chegg.com.chegghomework.classes.SourceCItem
import homework.chegg.com.chegghomework.databinding.FragmentMainBinding
import homework.chegg.com.chegghomework.ui.main.adapter.ArticlesRecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainFragment : Fragment() {
    var articlesList: ArrayList<Any> = ArrayList()
    lateinit var layoutManager: LinearLayoutManager
    private val mainVM: MainVM by viewModels()
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
           buildUI()
        listenToSwipe()
    }

    private fun buildUI() {
        //start shimmer effect when reload data

        binding.rvArticles.visibility = View.GONE
        binding.shimmerFrameLayout.startShimmerAnimation()
        CoroutineScope(Main).launch {
            mainVM.loadArticles().observe(viewLifecycleOwner, {

                if (it != null) {

                    articlesList.clear()
                    articlesList.addAll(it)

                    layoutManager = LinearLayoutManager(requireContext())
                    binding.rvArticles.layoutManager = layoutManager
                    binding.rvArticles.adapter =
                        ArticlesRecyclerView(articlesList, requireContext())

                    binding.shimmerFrameLayout.stopShimmerAnimation()
                    binding.shimmerFrameLayout.visibility = View.GONE
                    binding.rvArticles.visibility = View.VISIBLE
                } else {
                    binding.shimmerFrameLayout.stopShimmerAnimation()
                    binding.shimmerFrameLayout.visibility = View.GONE
                    //in case of some error happened notify to the user
                    notifyAboutError()
                }
            })
        }

    }



    //reload data by swipe
      private fun listenToSwipe() {
          binding.swipeRefresh.setOnRefreshListener {
              binding.shimmerFrameLayout.visibility = View.VISIBLE
              buildUI()
              binding.swipeRefresh.isRefreshing = false
          }
      }

    //bounce up snack bar with error msg
    private fun notifyAboutError() {
        CoroutineScope(Dispatchers.Main).launch {
            val view: View = binding.frameLayoutMain
            val snackbar: Snackbar = Snackbar.make(
                view,
                "oops something happened, try again",
                Snackbar.LENGTH_SHORT
            )
            val snackBarView = snackbar.view
            snackBarView.setBackgroundColor(Color.parseColor("#D83025"))
            snackbar.show()

        }
    }


}