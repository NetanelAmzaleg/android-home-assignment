package homework.chegg.com.chegghomework.classes

data class Articlewrapper(
    val description: String?,
    val header: String?
)