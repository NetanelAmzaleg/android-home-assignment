package homework.chegg.com.chegghomework.io.ioClass

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(indices = [Index(value = ["id"], unique = true)])
@Parcelize
data class TimeEntity(

    var source: String?,
    var currentTime: String?,
    @NonNull
    @PrimaryKey(autoGenerate = true)
    val id: Int?
): Parcelable



