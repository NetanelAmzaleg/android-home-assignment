package homework.chegg.com.chegghomework.di

import android.content.Context
import androidx.room.Room

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import homework.chegg.com.chegghomework.io.RoomDB
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {



    @Singleton // Tell Dagger-Hilt to create a singleton accessible everywhere in ApplicationCompenent (i.e. everywhere in the application)
    @Provides
    fun provideRoomDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        RoomDB::class.java,
        RoomDB.DATABASE_NAME
    ).fallbackToDestructiveMigration().build() // The reason we can construct a database for the repo


    @Singleton
    @Provides
    fun provideRoomDAO(roomDB: RoomDB) = roomDB.roomDao()
}


