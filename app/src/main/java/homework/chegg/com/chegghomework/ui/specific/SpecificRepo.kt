package homework.chegg.com.chegghomework.ui.specific

import androidx.lifecycle.MutableLiveData
import homework.chegg.com.chegghomework.classes.Innerdata
import homework.chegg.com.chegghomework.classes.SourceAObj
import homework.chegg.com.chegghomework.classes.SourceC
import homework.chegg.com.chegghomework.io.RoomDao
import homework.chegg.com.chegghomework.ui.Consts
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SpecificRepo @Inject constructor(

    private val roomDao: RoomDao

) {

    fun loadArticles(source : String): Flow<List<Any>?> {

        //Load source from DB
        return flow {
            try {


             when(source) {

                 Consts.DATA_SOURCE_A_URL->{
                    var list = roomDao.getSourceA()

                    emit(list)
                 }

                 Consts.DATA_SOURCE_B_URL->{
                     var list = roomDao.getSourceB()
                     emit(list)
                 }


                 Consts.DATA_SOURCE_C_URL->{
                     var list = roomDao.getSourceC()
                     emit(list)
                 }
             }


            } catch (e: Exception) {
               emit(null)
            }

        }
    }
}