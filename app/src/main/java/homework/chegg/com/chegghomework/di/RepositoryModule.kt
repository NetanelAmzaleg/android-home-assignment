package homework.chegg.com.chegghomework.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import homework.chegg.com.chegghomework.io.RoomDao
import homework.chegg.com.chegghomework.network.CacheInterceptorSourceA
import homework.chegg.com.chegghomework.ui.main.MainRepo
import homework.chegg.com.chegghomework.ui.specific.SpecificRepo
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideMainRepo(
        roomDao: RoomDao
    ): MainRepo{
        return MainRepo(roomDao)
    }


    @Singleton
    @Provides
    fun provideSpecificRepo(
        roomDao: RoomDao
    ): SpecificRepo{
        return SpecificRepo(roomDao)
    }
}