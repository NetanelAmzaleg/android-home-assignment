package homework.chegg.com.chegghomework.ui

object Consts {
    const val DATA_SOURCE_A_URL = "DATA_SOURCE_A_URL"
    const val DATA_SOURCE_B_URL = "DATA_SOURCE_B_URL"
    const val DATA_SOURCE_C_URL = "DATA_SOURCE_C_URL"
    const val DATA_SOURCE_D_URL = "DATA_SOURCE_D_URL"
    const val DATA_SOURCE_E_URL = "DATA_SOURCE_D_URL"
    const val DATA_SOURCE_F_URL = "DATA_SOURCE_D_URL"
    const val DATA_SOURCE_G_URL = "DATA_SOURCE_D_URL"
    const val DATA_SOURCE_H_URL = "DATA_SOURCE_D_URL"

    const val DATA_SOURCE_A_CACHE = 60000*5
    const val DATA_SOURCE_B_CACHE = 60000*30
    const val DATA_SOURCE_C_CACHE = 60000*60

}