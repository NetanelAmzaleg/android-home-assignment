package homework.chegg.com.chegghomework.ui.specific

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import homework.chegg.com.chegghomework.databinding.FragmentSpecificSourceBinding
import homework.chegg.com.chegghomework.ui.main.adapter.ArticlesRecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SpecificSourceFragment : Fragment() {
    var articlesList: ArrayList<Any> = ArrayList()
    lateinit var layoutManager: LinearLayoutManager
    private val specificVM: SpecificVM by viewModels()
    private var _binding: FragmentSpecificSourceBinding? = null
    private val binding get() = _binding!!
    lateinit var source : String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSpecificSourceBinding.inflate(inflater, container, false)
        return binding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //handle to source type by receive it from the income bundle
        source = requireArguments().getString("source")!!
        buildUI()
    }


    private fun buildUI() {
        //start shimmer effect when reload data
        binding.rvArticlesSpecific.visibility = View.GONE
        binding.shimmerFrameLayout.startShimmerAnimation()
        CoroutineScope(Dispatchers.Main).launch {
            specificVM.loadArticles(source = source).observe(viewLifecycleOwner, {

                if (it != null) {

                    articlesList.clear()
                    articlesList.addAll(it)

                    layoutManager = LinearLayoutManager(requireContext())
                    binding.rvArticlesSpecific.layoutManager = layoutManager
                    binding.rvArticlesSpecific.adapter =
                        ArticlesRecyclerView(articlesList, requireContext())

                    binding.shimmerFrameLayout.stopShimmerAnimation()
                    binding.shimmerFrameLayout.visibility = View.GONE
                    binding.rvArticlesSpecific.visibility = View.VISIBLE
                } else {
                    binding.shimmerFrameLayout.stopShimmerAnimation()
                    binding.shimmerFrameLayout.visibility = View.GONE
                    //in case of some error happened notify to the user
                    notifyAboutError()
                }
            })
        }

    }




    //bounce up snack bar with error msg
    private fun notifyAboutError() {
        CoroutineScope(Dispatchers.Main).launch {
            val view: View = binding.frameLayoutMain
            val snackbar: Snackbar = Snackbar.make(
                view,
                "oops something happened, try again",
                Snackbar.LENGTH_SHORT
            )
            val snackBarView = snackbar.view
            snackBarView.setBackgroundColor(Color.parseColor("#D83025"))
            snackbar.show()

        }
    }
}