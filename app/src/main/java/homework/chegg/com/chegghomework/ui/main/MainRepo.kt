package homework.chegg.com.chegghomework.ui.main

import homework.chegg.com.chegghomework.classes.*
import homework.chegg.com.chegghomework.io.RoomDao
import homework.chegg.com.chegghomework.io.ioClass.SourceAEntity
import homework.chegg.com.chegghomework.io.ioClass.SourceBEntity
import homework.chegg.com.chegghomework.io.ioClass.SourceCEntity
import homework.chegg.com.chegghomework.io.ioClass.TimeEntity
import homework.chegg.com.chegghomework.network.RetroService
import homework.chegg.com.chegghomework.ui.Consts
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import retrofit2.Response
import java.sql.Timestamp
import javax.inject.Inject

class MainRepo @Inject constructor(
    private val roomDao: RoomDao
) {


    fun loadArticles(): Flow<ArrayList<Any>?> {

        return channelFlow {
            try {
                var listArtitcals = ArrayList<Any>()
                withContext(IO) {

                    //Get the last api call for every source
                    var lastCallA = roomDao.getSourceTime(Consts.DATA_SOURCE_A_URL)
                    var lastCallB = roomDao.getSourceTime(Consts.DATA_SOURCE_B_URL)
                    var lastCallC = roomDao.getSourceTime(Consts.DATA_SOURCE_C_URL)

                    //check if time pass and then use the api or use inner DB
                    if (lastCallA == null || checkIfPassTime(
                            lastCall = lastCallA,
                            Consts.DATA_SOURCE_A_CACHE
                        )
                    ) {
                        val resultA: Deferred<ArrayList<SourceAEntity>> = async {
                            loadDataSourceAny(Consts.DATA_SOURCE_A_URL) as ArrayList<SourceAEntity>
                        }
                        listArtitcals.addAll(resultA.await())
                    } else {
                        listArtitcals.addAll(roomDao.getSourceA())
                    }

                    //check if time pass and then use the api or use inner DB
                    if (lastCallB == null || checkIfPassTime(
                            lastCall = lastCallB,
                            Consts.DATA_SOURCE_B_CACHE
                        )
                    ) {
                        val resultB: Deferred<ArrayList<SourceBEntity>> = async {
                            loadDataSourceAny(Consts.DATA_SOURCE_B_URL) as ArrayList<SourceBEntity>
                        }
                        listArtitcals.addAll(resultB.await())
                    } else {
                        listArtitcals.addAll(roomDao.getSourceB())
                    }
                    //check if time pass and then use the api or use inner DB
                    if (lastCallC == null || checkIfPassTime(
                            lastCall = lastCallC,
                            Consts.DATA_SOURCE_C_CACHE
                        )
                    ) {
                        val resultC: Deferred<ArrayList<SourceCEntity>> = async {
                            loadDataSourceAny(Consts.DATA_SOURCE_C_URL) as ArrayList<SourceCEntity>
                        }
                        listArtitcals.addAll(resultC.await())
                    } else {
                        listArtitcals.addAll(roomDao.getSourceC())
                    }


                    //In case of another sources
                    val resultD: Deferred<ArrayList<Any>> = async {
                        loadDataSourceD()
                    }
                    //In case of another sources
                    listArtitcals.addAll(resultD.await())
                    //In case of another sources
                    listArtitcals.addAll(resultD.await())
                    listArtitcals.addAll(resultD.await())
                    listArtitcals.addAll(resultD.await())
                    listArtitcals.addAll(resultD.await())
                    listArtitcals.addAll(resultD.await())
                    channel.send(listArtitcals)


                }


            } catch (e: Exception) {
                channel.send(null)
            }

        }
    }

    private suspend fun loadDataSourceAny(source: String): ArrayList<*> {
        //Handle the data source and in case use another source just put it here
        when (source) {

            Consts.DATA_SOURCE_A_URL -> {
                return checkResponse(RetroService.api.loadDataSourceA())

            }

            Consts.DATA_SOURCE_B_URL -> {
                return checkResponse(RetroService.api.loadDataSourceB())
            }


            Consts.DATA_SOURCE_C_URL -> {
                return checkResponse(RetroService.api.loadDataSourceC())
            }
            else -> ArrayList<SourceC>()
        }


        return ArrayList<SourceC>()

    }


    private suspend fun checkResponse(result: Response<*>): ArrayList<*> {
//check the response and by type and in case use another source just put it here
        return if (result.isSuccessful) {
            if (result.body() != null) {
                when (result.body()) {
                    is SourceA -> {
                        val stories = (result.body() as SourceA).stories!!
                        handleDBSourcA(stories)

                    }
                    is SourceB -> {
                        val innerdata = (result.body() as SourceB).metadata!!.innerdata!!
                        handleDBSourcB(innerdata)

                    }
                    is SourceC -> {
                        val sourceCList = (result.body() as SourceC)
                        handleDBSourcC(sourceCList)

                    }
                    else -> {
                        ArrayList()
                    }
                }
            } else {
                ArrayList()
            }
        } else {

            val arrayList = ArrayList<SourceC>()
            arrayList
        }

    }

    //Handle DB fetch and in case use another source just put it here
    private suspend fun handleDBSourcC(sourceCList: SourceC): ArrayList<SourceCEntity> {
        var sourceCEntityList: ArrayList<SourceCEntity> = ArrayList()
        //Convert the object from api data class to entity class
        sourceCList.forEach { it ->
            var sourceCEntity =
                SourceCEntity(it.image, it.subLine1, it.subline2, it.topLine, null)
            sourceCEntityList.add(sourceCEntity)

        }
        //Rewrite time source
        roomDao.deleteSourceTime(Consts.DATA_SOURCE_C_URL)
        roomDao.insertSourceTime(
            TimeEntity(
                Consts.DATA_SOURCE_C_URL,
                System.currentTimeMillis().toString(),
                null
            )
        )
        roomDao.deleteSourceC()
        roomDao.insertSourceCList(sourceCEntityList)
        return sourceCEntityList
    }

    private suspend fun handleDBSourcB(innerdata: List<Innerdata>): ArrayList<SourceBEntity> {
        val sourceBEntityList: ArrayList<SourceBEntity> = ArrayList()
        innerdata.forEach { inner ->
            var sourceBEntity = SourceBEntity(
                inner.articlewrapper!!.description,
                inner.articlewrapper.header,
                inner.aticleId,
                inner.picture,
                null
            )
            sourceBEntityList.add(sourceBEntity)

        }
        roomDao.deleteSourceTime(Consts.DATA_SOURCE_B_URL)
        roomDao.insertSourceTime(
            TimeEntity(
                Consts.DATA_SOURCE_B_URL,
                System.currentTimeMillis().toString(),
                null
            )
        )
        roomDao.deleteSourceB()
        roomDao.insertSourceBList(sourceBEntityList)
        return sourceBEntityList
    }

    private suspend fun handleDBSourcA(stories: List<SourceAObj>): ArrayList<SourceAEntity> {
        var sourceAEntityList: ArrayList<SourceAEntity> = ArrayList()
        stories.forEach { story ->
            var sourceAEntity =
                SourceAEntity(story.imageUrl, story.subtitle, story.title, null)
            sourceAEntityList.add(sourceAEntity)

        }

        roomDao.deleteSourceTime(Consts.DATA_SOURCE_A_URL)
        roomDao.insertSourceTime(
            TimeEntity(
                Consts.DATA_SOURCE_A_URL,
                System.currentTimeMillis().toString(),
                null
            )
        )

        roomDao.deleteSourceA()
        roomDao.insertSourceAList(sourceAEntityList)
        return sourceAEntityList
    }


    private suspend fun loadDataSourceD(): ArrayList<Any> {
        val response = RetroService.api.loadDataSourceD()
        return if (response.isSuccessful) {
            response.body()!!
        } else {
            ArrayList()
        }
    }


    //Check if time pass and return true or false
    private fun checkIfPassTime(lastCall: String, sourceTime: Int): Boolean {
        return   Timestamp(System.currentTimeMillis()).after(Timestamp(lastCall.toLong() + sourceTime.toLong()))
    }


}