package homework.chegg.com.chegghomework.io.ioClass

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import homework.chegg.com.chegghomework.classes.Articlewrapper
import kotlinx.android.parcel.Parcelize

@Entity(indices = [Index(value = ["id"], unique = true)])
@Parcelize
data class SourceBEntity(
    var description: String?,
    var header: String?,
    var aticleId: Int?,
    var picture: String?,
    @NonNull
    @PrimaryKey(autoGenerate = true)
    val id: Int?
): Parcelable



