package homework.chegg.com.chegghomework.network




import homework.chegg.com.chegghomework.classes.SourceA
import homework.chegg.com.chegghomework.classes.SourceB
import homework.chegg.com.chegghomework.classes.SourceC
import retrofit2.Response


import retrofit2.http.GET



interface Api {



    @GET("android_homework_datasourceA.json")
    suspend fun loadDataSourceA(): Response<SourceA>

    @GET("android_homework_datasourceB.json")
    suspend fun loadDataSourceB(): Response<SourceB>

    @GET("android_homework_datasourceC.json")
    suspend fun loadDataSourceC(): Response<SourceC>

    //In case of another sources
    @GET("android_homework_datasourceD.json")
    suspend fun loadDataSourceD(): Response<ArrayList<Any>>
    @GET("android_homework_datasource.json")
    suspend fun loadDataSourceE(): Response<ArrayList<Any>>
    @GET("android_homework_datasourceD.json")
    suspend fun loadDataSourceF(): Response<ArrayList<Any>>
    @GET("android_homework_datasourceD.json")
    suspend fun loadDataSourceG(): Response<ArrayList<Any>>
    @GET("android_homework_datasourceD.json")
    suspend fun loadDataSourceH(): Response<ArrayList<Any>>
    @GET("android_homework_datasourceD.json")
    suspend fun loadDataSourceI(): Response<ArrayList<Any>>




}