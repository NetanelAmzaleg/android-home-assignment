package homework.chegg.com.chegghomework.network

import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.CacheControl
import java.util.concurrent.TimeUnit


class CacheInterceptorSourceB : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        val response = chain.proceed(chain.request())

        val cacheControl = CacheControl.Builder()
            .maxAge(30, TimeUnit.MINUTES) // 5 minutes cache
            .build()

        return response.newBuilder()
            .header("Cache-Control", cacheControl.toString())
            .build()
    }
}